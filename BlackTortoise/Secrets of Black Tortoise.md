﻿Category: **Forensics**

**Title: Secrets of Black Tortoise** 

**Description:**
The Black Tortoise is one of the Four Symbols of Chinese Astronomy, representing longevity, strength, and protection. This celestial guardian is a tortoise interconnected with a snake. Just like the tortoise and snake hibernate in the winter, a message is hidden within the piece of art, waiting to be uncovered. The file is given here:  [image](https://drive.google.com/file/d/16j5cADh86BrpJsD2H_EjrqUqgtncPmfP/view?usp=sharing) 

**Hint**: They call me different names in different mythologies. Find my home and you shall know my common name.

**Intended Learning and outcome:** 

With the given challenge, I was able to learn about ExifTool and Base64 encode/decode. 

ExifTool can be used to learn about a file. The metadata on a file can be utilised for various purpose like analyse, interpret, add additional information, and even extract information to look for hidden keywords or meanings. Here, ExifTool plays a vital role to extract additional information hidden within the image's metadata, leading them to discover the encoded message.

Learn about cryptographic concept using Base64 encode decode. It will develop awareness on securing important information and teach a valuable lesson on how vulnerable the metadata is and how it can be easily manipulated. 

**Solution:** 

It talks about Black Tortoise constellation, famous in the Chinese mythology. The image shows nothing, but an image of a black tortoise twined with the snake, as the title name. The participant can analyse the data with the file properties, but not much can be seen. So, to deeply analyse and interpret the image, Exif tool is used. Under the description, we see some jargon text which is the first hint for the answer. It may be encoded, and we can use various online tool to decode its meaning. This is where base64 comes in. Upon decoding, the flag is revealed as "flag{Delphinus,NorthernHemisphere}". As hinted, constellations are named after different religions, cultures, and mythologies, and the task was to find the name and location of the constellation commonly used.

