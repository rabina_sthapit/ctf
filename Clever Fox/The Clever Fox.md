﻿**Category: Cryptography**

**Title: The Clever Fox**

**Description:** This cryptography challenge features a text file encrypted with the Substitution cipher. The task is to decode the text to discover the flag. Online tools can be used for Substitution cipher decryption. Here is the [text file](https://drive.google.com/file/d/14qxMI-jPVvKY_SV7Rtl7Ajz6C0f0mFIo/view?usp=drive_link).

**Hint**: The key is at your disposal but knowing "when" and "where" to use it is crucial in unlocking the solution.

**Intended Learning and outcome:** Through this challenge, it deepens the understanding of Substitution ciphers. By deciphering the encrypted text and uncovering the flag, it will enhance their problem-solving skills and learn to strategically apply cryptographic techniques. 

**Solution:**  

As we open the file, we see some scrambled words and there is a key at the starting of the text document. As we check the bottom part, there might be an obvious hint for our flag which is encrypted. As the description suggests substitution cipher, we are given a key which hints a jumbled letter of alphabet. By using any online tool that will decode the substitution cipher based on mono-alphabetic substitution, we get the flag as flag{lionthrone}.

